---
layout: about
permalink: /
title: S. Üsküdarlı
description: <a href="https://www.cmpe.bogazici.edu.tr">Computer Engineering Department, <a href="https://soslab.cmpe.bogazici.edu.tr">Complex Systems Lab (SoSLab)</a>, <a href="https://tabilab.cmpe.bogazici.edu.tr/">Text Analytics and BIoInformatics Lab (TABI Lab)</a> @ <a href="https://www.bogazici.edu.tr/">Boğaziçi University</a>.


profile:
  align: right
  image: prof_pic.jpg
  address: >
    <p>BM 35</p>
    <p>Tel: x7682</p>
    <p>suzan.uskudarli@bogazici.edu.tr</p>

news: false
social: false
---

<div> I am a computer scientist who is interested in the social impacts of computation, which 
covers multidisciplinary fields including social semantic web, knowledge representation, complex and social network analysis, computer-mediated cooperation & communication, virtual/digital environments, virtual reality, and human-computer interaction (HCI). My present focus areas are semantic web, social network analysis, virtual communities, visual languages, and digital inclusion.
</div>

<br/>
<div>
 I joined the Computer Engineering Department of Boğaziçi University in 2005 after working in California subsequent to receiving my PhD in <a href="https://www.uva.nl/en/about-the-uva/organisation/faculties/faculty-of-science/faculty-of-science.html">University of Amsterdam</a> related to the algebraic specification of visual lanugages.

</div>
