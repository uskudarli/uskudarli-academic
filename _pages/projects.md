---
layout: page
title: projects
permalink: /projects/
---

<h4>Projects</h4>

<div> The following are projects that I have been involved in. The dates indicate the project initiation. Ongoing projects are indiated.</div>

<h4> Project outcomes:</h4>

The following are two projects relevant to Turkish Natural Language Processing contributions:


<ul>
<li> <a href="https://github.com/boun-tabi-LMG/TURNA">TURNA</a> Turkish Encoder-Decoder Language Model. 
The model is available at <a href="https://github.com/boun-tabi-LMG/turkish-lm-tuner">TURNA Model at Hugging Face</a>.
You can try the demos at: <a href="https://huggingface.co/spaces/boun-tabi-LMG/TURNA">TURNA demo at Hugging Face</li>
<li> <a href="https://tulap.cmpe.boun.edu.tr/">TULAP: A Turkish Language Processing Platform that serves open source Turkish NLP resources developed at the Computer Engineering and Linguistic Departments at Boğaziçi University. This platform which serves research output products focus on accessible resources.  </a> </li>
</ul>


<h4> Projects that I have led:</h4>

<ul>
<li>Yapay Sinir Ağı Tabanlı Varlık Tanıma Sistemi (Neural Network Based Named Entity), (2020) <em> Bogazici Research Fund 18A01P3 (2018) </em> </li>
<li>Coğrafya Üzerindeki Hareket Alışkanlıklarının Sosyal Ağlarla Olan İlişkileri <em> Bogazici Research Fund 5709P (2010) </em> </li>
<li>Mikroblog Verilerinin Anlamsal Olarak Kullanılması (Semantic Application of Microblogging Content) <em> Bogazici Research Fund 09HA102P  (2009) </em> </li>
<li>Class Design in Engineering Courses <em> Hewlett-Packard Innovation in Education Grant (HPIIE)  (2010) </em> </li>
<li>Sosyal Ağlarda Çevrimiçi Toplumlar İçeriklerin Anlamlandırılması (Semantic Representation of Social Network Communities) <em> Bogazici Research Fund 08A103  (2008)</em></li>
<li>Katılımcı Anlamsal Mobil Bilgi Platformu (A Participatory Mobile Information Platform) <em> Bogazici Research Fund 07A107 (2007)</em> </li>
<li>Çokluortamlarda Yapılı ve Erişilebilir Bilgi Yönetim Platformu (Platform for a Structured Accessible Information Management System) -  <em> Bogazici Research Fund 06A101 (2006) </em>  </li>
</ul>

<h4>Projects that I have participated in as a resarcher:</h4>

<ul>                   
<li>Dilbilim Temelli Türkçe Doğal Dil İşleme Platformu <em> Bogazici Interdiscipliniary Research Fund (2020) - </em>  ongoing </li>
<li>Polarization or dialogue? A deep learning study of the Black lives matter and Me Too online social movement - <em> H2020 Id: 101028566 </em> (2021-2024)</li>
<li>OpenMaker: Harnessing the power of Digital Social Platforms to shake up makers and manufacturing entrepreneurs towards a European Open Manufacturing ecosystem -  <em> H2020 Id: 687941 </em> (2016)  </li>
<li>Semantic Keyword-based Search on Structured Data Sources (KEYSTONE) -  <em> EU COST IC1302 (2013) </em></li>
<li>CaReRa: Content Based Case Retrieval in Radiological Databases (Cases of LiverDiseases) - <em> TUBITAK 1001, Project #: 110E264 (2011) </em></li>
<li>CBIR4Liver: 3B Karaciğer BT İmgelerini Benzerlik Tabanlı Sorgulama (Content Based CT Image Retrieval for Liver Cases) -  <em> Bogazici Interdiscipliniary Research Fund (2010) </em> </li>
<li>m-öğrenme (Mobile Learning) - <em>Turkcell (2008) </em></li>
</ul>

<h4>Notable senior project :</h4>

<ul>
<li> Salih Furkan Akkurt worked on a collaborative web annotation tool. <a href="https://github.com/boun-tabilab-dip/Web-Annotation-Tool"> GitHub repository </a> </li>
<li> Hasan Öztürk, Alperen Değirmenci worked on the troublesome spelling of the de/da. They started with a research project titled "Deep learning techniques for handling Turkish clitics", which was followed by a project that focused on explanation titled "Explaining A Deep Learning Based Model for Spelling Correction of "de/da" Clitics in Turkish through Understanding Their Dependence to Various Regions of a Sentence". They developed a program to collect information about de/da spelling errors that was very well received.   <a href="https://www.cmpe.boun.edu.tr/tr/news/deda-spelling-corrector">dedatakintisi app news</a></li>
</ul>


