---
layout: page
permalink: /publications/
title: publications
---

{% bibliography --query @*[group=journal] %}
{% bibliography --query @*[group=conference] %}
{% bibliography --query @*[group=chapter] %}
{% bibliography --query @*[group=preprint] %}
{% bibliography --query @*[group=report] %}
