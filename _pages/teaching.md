---
layout: page
permalink: /teaching/
title: teaching
# description: 
---

<h3>Teaching Activities</h3>


<h4> Courses I have taught:</h4>

<div>
Fundamentals of Software Engineering,  Project Development in Software Engineering, Introduction to Programming,  Operating Systems and Their Use, Introduction to Computers, Web Programming, Cooperation Technologies in the Context of Computer Science, Social Semantic Web
</div>

<h4> Supervised PhD students:</h4>

<ul>
<li> Onur Güngör, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=9MiDp3x86xrwjpi5-14w-bRDFGYQ6nuc4yZin8mXtneKiM9L8AopEZ0luh0XWUoU">Neural Named Entity Recognition for Morphologically Rich Languages (Biçimbilimsel Açıdan Zengin Dillerde Sinir Ağı Tabanlı Varlık İsmi Tanıma)</a> (2021) </li>
<li> Ahmet Yıldırım,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=fl0Kw4p1rmMDotyKRdYv1OPS9_NkQ9F2gskfuTYv53Gm3iUF1XhxO8W5itcDhone">Topic identification within microblog post collections (Kısa ileti kümelerinde konu algılama)</a>  (2020)</li>
</ul>

<h4> Supervised MS students:</h4>

<ul>
<li>Abdullah Atakan Güney, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=qVqOZFj2DwNmvdf1oGFYiED4OQWBceapEASFqz3txkMJFk6cAZ40JwAsWEuG9_U5">A knowledge-graph based graph neural network model to identify topics in short texts (Kısa yazılar içerisindeki konuları tanımlamak için bilgi grafiği tabanlı çizge sinir ağı modeli)</a>(2022)</li>
<li>Berkay Ataman, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=kScA8XnrRb0WogX-qPGFkrQof4glnPaFoDOdh4W6ktfoaD8PqjXNHbujnBLywaPJ">An ontology based representation of semantic annotations for biomedical relations extracted from scientific documents (Bilimsel belgelerden çıkarılmış biyomedikal ilişkiler için anlamsal açıklamaların ontoloji temelli temsili)</a>(2022)</li>
<li>Emre Hoş,<a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=9MiDp3x86xrwjpi5-14w-QnLbAGKdXVWlPH8UbMXSVks6uwAcFBS5aTRES7Evp3t">Accessibility on the Web through Semantic and Social Renarrations</a>(2021) </li>
<li>Erdem Beğenilmiş, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=7lOJX8w_8PRQU1mSHU6-jrR50mlFyuhxLW1SRxFNSmzq_zO3bQ9xmUVl7vMcfKAt">Detection of organized behaviours on Twitter (Organize davranışların Twitter'da belirlenmesi)</a> (2017)</li>
<li>Murat Seyhan, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=cbOXH84ZayrLjc0tI-QXKi8gCmO_CKryqusF441oujwP0LJ5-UUXYinUPgmecJLD">An ontology based framework for creating purposeful online communities</a> (2016) </li>
<li>Emrah Güder, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=cbOXH84ZayrLjc0tI-QXKjvHX3nVdFJ1Swt-dDbmMCT3Nu_ykd9xxUtDEQnZBIZT"> Increasing accessibility of web content via semantic renarration</a> (2016) </li>
<li>Hakan Anit, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=vVNzTGHHhjH-u3WMToxQ-kWU4iObSxT-59iLTyfuoEKOeT0ogZKnVGectEg8VPju"> Emergency situation notification based on social networks for mobile devices</a> (2013) </li>
<li>Nadin Kökciyan, <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=EEdeQgIdFRxX5NbvVau-AphO0aEhLeKtZF5uq-qvfUjICy4NJGLJLGx2QXLjWFNc">WeFlow: We follow the flow</a> (2012) </li>
<li>Hüseyin Burak Çelebi,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=RYan9_S-Z7Eir3xdWGXBiLWcZdTniBKZeWZvg_9jrVP8A20IH3lj5oWQuDP-tg85">A content based microblogger recommendation model</a> (2012) </li>
<li>Murat Kalender,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=veR1mHu9yoWjwcVUjCEoPIPaqg83mdEsdR1kTcXymJhwewuLz3ViiL0pfSwcA5WU">Automated semantic tagging of text documents</a> (2010) </li>
<li>Ece Aksu Değirmencioğlu,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=veR1mHu9yoWjwcVUjCEoPKScdpMA9hI7lCTcc1SBbfp27KV7Eu6N3PR90zvN2qMB"> Exploring area-specific microblogger social networks</a> (2010) </li>
<li>Emre Yurtsever,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=veR1mHu9yoWjwcVUjCEoPAcpUzsZ613kRo1UyGAJ_YHbVvllLILjV15n4gzO8QO4"> SweetTweet: A semantic analysis for microblogging environments</a> (2010) </li>
<li>  Mehmet Fatih Köksal,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=NtBAevXNhYaNqJFoAcdBdomPNELPQlX_Mc_UqkYrHbNZOKveOdZYpDhxPYKEbmAJ">Screen-replay: A tool for tracking how students develop programs with HTDP</a> (2010) </li>
<li>Duygu Saide Akman,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=veR1mHu9yoWjwcVUjCEoPKS_5o1m4kYI__l8_O8Zcv4aqt1Wf6e5Ws_QuEX1sHr1">Revealing microblogger interests by analyzing contributions</a> (2010) </li>
<li>Okay Aslan,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=veR1mHu9yoWjwcVUjCEoPFE01dh0lY77R1cP0qS1O-zaZCMcgaD7qJopkXmEd31o">An analysis of news on microblogging systems</a> (2010) </li>
<li>Dağhan Dinç,  <a href="https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=UPP_Zu9isEmWGFXFCBYasX7Zy_4J7p4hSyLQnze-YcxwxyFB540RzVtSNE1tcbXH">Web environment to support teaching introductory programming</a> (2008) </li>
</ul>


